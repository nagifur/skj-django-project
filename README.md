# Django Art Gallery

**SKJ project**
Web app, based on the Python Django dynamic web framework.
The web app functions as a management software for
an art gallery administrators & artists.

## Files & Folders

- src/ #source files
  - static/
    - img/ # artworks
    - css/ #style sheet
  - templates/ # html pages

## Prerequisites

- Python 3.10
- Virtual Enviroment (recommended)
- Microsoft Windows - *Linux systems curretnly unsupported*

## Installation

- `git clone https://gitlab.com/nagifur/skj-django-project.git`
- `pip install -r requirements.txt`
- `python manage.py runserver`

## Pylint Report

Latest Pylint result report is automatically generated thorugh GitLab pages.
You can find it on:

<https://nagifur.gitlab.io/skj-django-project/>
