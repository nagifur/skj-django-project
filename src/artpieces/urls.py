from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('artdetails', views.artdetails, name="artdetails"),
    path('exhibitions', views.exhibitions, name="exhibitions"),
    path('submitArt', views.submitArt, name="submitArt"),
    path('login', views.login, name="login"),
    path('orders', views.orders, name="orders"),
    path('customers', views.customers, name="customers"),
    path('artists', views.artists, name="artists"),
    path('exhibitiondetails', views.exhibitiondetails, name="exhibitiondetails"),
    path('registerart', views.registerart, name="registerart"),
    path('customeredit', views.customeredit, name="customeredit"),
    path('artistedit', views.artistedit, name="artistedit"),
    path('newreview', views.newreview, name="newreview"),
    path('newExhibition', views.newExhibition, name="newExhibition"),
]
