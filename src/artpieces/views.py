from django.shortcuts import render
from django.http import HttpResponse
from unicodedata import name
from django.views.decorators.csrf import csrf_exempt

from .models import *
import shutil
from .globals import *
import os

# Create your views here.


def index(request):
    artworks = list(Artpiece.objects.all())
    filtered = artworks

    for x in artworks:
        x.SetPath()

    if request.method == 'GET':

        # Filter name
        if request.GET.get('tb_search', '-1') != '-1':
            filtered = []
            for x in artworks:
                if request.GET.get('tb_search', '-1') in x.name:
                    filtered.append(x)
        if request.GET.get('cb_price', '-1') != '-1':
            if request.GET.get('cb_price', '-1') == 'asc':
                filtered.sort(key=lambda x: x.price)
            else:
                filtered.sort(key=lambda x: x.price, reverse=True)
        if request.GET.get('delete', '-1') != '-1':
            Artpiece.objects.filter(
                pk=request.GET.get(
                    'delete', '-1')).delete()
            filtered = list(Artpiece.objects.all())
            for x in filtered:
                x.SetPath()

    return render(request, 'index.html', {'artworks': filtered,
                                          'loggedId': loggedId,
                                          'loggedUsername': loggedUsername,
                                          'isAdmin': isAdmin,
                                          'isArtist': isArtist
                                          })


def artdetails(request):
    selectedID = request.GET.get('id', '-1')
    selectedArt = Artpiece.objects.get(pk=selectedID)
    selectedArt.SetPath()

    reviews = list(Review.objects.filter(artid=selectedID))

    # Registered to an Exhibtion
    if request.GET.get('registered', 'false') == 'true':
        newRegistration = ExhibitionArtpiece(
            artid=selectedArt, exhibitionid=Exhibition.objects.get(
                pk=request.GET['cb_exhibitions']))
        newRegistration.save()

    return render(request,
                  'artdetails.html',
                  {'selectedArt': selectedArt,
                   'reviews': reviews,
                   'loggedId': loggedId,
                   'loggedUsername': loggedUsername,
                   'isAdmin': isAdmin,
                   'isArtist': isArtist})


def registerart(request):
    selectedID = request.GET.get('id', '-1')
    selectedArt = Artpiece.objects.get(pk=selectedID)
    selectedArt.SetPath()
    exhibitions = Exhibition.objects.all()

    return render(request,
                  'registerart.html',
                  {'selectedArt': selectedArt,
                   'exhibitions': exhibitions,
                   'loggedId': loggedId,
                   'loggedUsername': loggedUsername,
                   'isAdmin': isAdmin,
                   'isArtist': isArtist})


def header(request):
    render(request, 'header.html')


def exhibitiondetails(request):
    selectedID = request.GET.get('id', '-1')
    exhibition = Exhibition.objects.get(pk=selectedID)

    arts = list(ExhibitionArtpiece.objects.filter(exhibitionid=selectedID))

    return render(request,
                  'exhibitiondetails.html',
                  {'exhibition': exhibition,
                   'arts': arts,
                   'loggedId': loggedId,
                   'loggedUsername': loggedUsername,
                   'isAdmin': isAdmin,
                   'isArtist': isArtist})


def exhibitions(request):
    exhibitions = Exhibition.objects.all()

    if request.GET.get('delete', '-1') != '-1':
        Exhibition.objects.filter(pk=request.GET.get('delete', '-1')).delete()
        exhibitions = Exhibition.objects.all()

    return render(request,
                  'exhibitions.html',
                  {'exhibitions': exhibitions,
                   'loggedId': loggedId,
                   'loggedUsername': loggedUsername,
                   'isAdmin': isAdmin,
                   'isArtist': isArtist})


@csrf_exempt
def newreview(request):
    selectedID = request.GET.get('id', '-1')
    selectedArt = Artpiece.objects.get(pk=selectedID)

    if request.method == 'POST':
        newReview = Review(stars=request.POST['tb_stars'],
                           comment=request.POST['tb_comment'],
                           customerid=Customer.objects.get(pk=loggedId),
                           artid=selectedArt)
        newReview.save()
    return render(request, 'newreview.html', {
        'loggedId': loggedId,
        'loggedUsername': loggedUsername,
        'isAdmin': isAdmin,
        'isArtist': isArtist
    })


def orders(request):
    orders = Orders.objects.all()
    if request.method == 'GET':
        # Delete
        if request.GET.get('delete', '-1') != '-1':
            Orders.objects.filter(pk=request.GET.get('delete', '-1')).delete()

    return render(request, 'orders.html', {'orders': orders,
                                           'loggedId': loggedId,
                                           'loggedUsername': loggedUsername,
                                           'isAdmin': isAdmin,
                                           'isArtist': isArtist
                                           })


def customeredit(request):

    return render(request, 'customeredit.html', {
        'loggedId': loggedId,
        'loggedUsername': loggedUsername,
        'isAdmin': isAdmin,
        'isArtist': isArtist
    })


def artistedit(request):

    return render(request, 'artistedit.html', {
        'loggedId': loggedId,
        'loggedUsername': loggedUsername,
        'isAdmin': isAdmin,
        'isArtist': isArtist
    })


@csrf_exempt
def customers(request):
    customers = Customer.objects.all()

    if request.method == 'GET':
        # Delete
        if request.GET.get('delete', '-1') != '-1':
            Customer.objects.filter(
                pk=request.GET.get(
                    'delete', '-1')).delete()
        # Edit form
        if request.GET.get('edit', '-1') != '-1':
            customerId = request.GET.get('edit', '-1')
            customer = Customer.objects.get(pk=customerId)
            return render(request,
                          'customeredit.html',
                          {'customer': customer,
                           'loggedId': loggedId,
                           'loggedUsername': loggedUsername,
                           'isAdmin': isAdmin,
                           'isArtist': isArtist})
        # Update Edited
        if request.GET.get('edited', '-1') == 'true':
            Customer.objects.filter(
                pk=request.GET.get(
                    'editId',
                    '-1')).update(
                first_name=request.GET.get(
                    'tb_firstname',
                    '-1'),
                last_name=request.GET.get(
                    'tb_lastname',
                    '-1'),
                email=request.GET.get(
                    'tb_email',
                    '-1'),
                phone=request.GET.get(
                    'tb_phone',
                    '-1'))

    return render(request, 'customers.html', {'customers': customers,
                                              'loggedId': loggedId,
                                              'loggedUsername': loggedUsername,
                                              'isAdmin': isAdmin,
                                              'isArtist': isArtist
                                              })


def artists(request):
    artists = Artist.objects.all()

    if request.method == 'GET':
        # Delete
        if request.GET.get('delete', '-1') != '-1':
            Artist.objects.filter(pk=request.GET.get('delete', '-1')).delete()
        # Edit form
        if request.GET.get('edit', '-1') != '-1':
            artistId = request.GET.get('edit', '-1')
            artist = Artist.objects.get(pk=artistId)
            return render(request,
                          'artistedit.html',
                          {'artist': artist,
                           'loggedId': loggedId,
                           'loggedUsername': loggedUsername,
                           'isAdmin': isAdmin,
                           'isArtist': isArtist})
        # Update Edited
        if request.GET.get('edited', '-1') == 'true':
            Artist.objects.filter(
                pk=request.GET.get(
                    'editId',
                    '-1')).update(
                first_name=request.GET.get(
                    'tb_firstname',
                    '-1'),
                last_name=request.GET.get(
                    'tb_lastname',
                    '-1'),
                email=request.GET.get(
                    'tb_email',
                    '-1'),
                nickname=request.GET.get(
                    'tb_nickname',
                    '-1'))
    return render(request, 'artists.html', {'artists': artists,
                                            'loggedId': loggedId,
                                            'loggedUsername': loggedUsername,
                                            'isAdmin': isAdmin,
                                            'isArtist': isArtist
                                            })


@csrf_exempt
def submitArt(request):
    if request.method == 'POST':
        newArt = Artpiece(name=request.POST['tb_title'],
                          price=request.POST['tb_price'],
                          style=request.POST['tb_style'],
                          type=request.POST['tb_type'],
                          sold=False,
                          artistid=Artist.objects.get(pk=loggedId)
                          )
        newArt.save()
        art = Artpiece.objects.all().last()
        shutil.copyfile(
            f"static\\img\\{request.POST['imgfile']}",
            f"static\\img\\{art.artid}.jpg")
    return render(request,
                  'submitArt.html',
                  {'loggedId': loggedId,
                   'loggedUsername': loggedUsername,
                   'isAdmin': isAdmin,
                   'isArtist': isArtist,
                   'artist': Artist.objects.get(pk=loggedId)})


@csrf_exempt
def newExhibition(request):
    if request.method == 'POST':
        newExh = Exhibition(starting_date=request.POST['tb_starting'],
                            ending_date=request.POST['tb_ending'],
                            address=request.POST['tb_address'],
                            number_of_visitors=request.POST['tb_visitors']
                            )
        newExh.save()

    return render(request,
                  'newExhibition.html',
                  {'loggedId': loggedId,
                   'loggedUsername': loggedUsername,
                   'isAdmin': isAdmin,
                   'isArtist': isArtist,
                   })


@csrf_exempt
def login(request):
    global loggedId
    global loggedUsername
    global isAdmin
    global isArtist
    error = ""
    if request.method == 'POST':
        try:
            user = Artuser.objects.get(username=request.POST['tb_username'])
            if user.password == request.POST['tb_password']:
                loggedId = user.userid
                loggedUsername = user.username
                isAdmin = user.admin
                isArtist = user.artist
            else:
                error = "Wrong Password"
        except BaseException:
            error = "Wrong Username or Password"

    return render(request, 'login.html', {'loggedId': loggedId,
                                          'loggedUsername': loggedUsername,
                                          'isAdmin': isAdmin,
                                          'isArtist': isArtist,
                                          'error': error
                                          })


def footer(request):
    render(request, 'footer.html')
