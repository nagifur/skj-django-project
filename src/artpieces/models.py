# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or
# field names.
from django.db import models


class Artpiece(models.Model):
    # Field name made lowercase.
    artid = models.AutoField(db_column='artID', primary_key=True)
    name = models.CharField(
        max_length=50,
        db_collation='SQL_Latin1_General_CP1_CI_AS')
    price = models.DecimalField(max_digits=19, decimal_places=4)
    style = models.CharField(max_length=50,
                             db_collation='SQL_Latin1_General_CP1_CI_AS')
    type = models.CharField(
        max_length=20,
        db_collation='SQL_Latin1_General_CP1_CI_AS')
    sold = models.BooleanField()
    # Field name made lowercase.
    artistid = models.ForeignKey(
        'Artist',
        models.CASCADE,
        db_column='artistID')
    imgpath = ""

    def SetPath(self):
        self.imgpath = "img/" + str(self.artid) + ".jpg"

    class Meta:
        managed = False
        db_table = 'ArtPiece'


class Artuser(models.Model):
    # Field name made lowercase.
    userid = models.AutoField(db_column='userID', primary_key=True)
    username = models.CharField(
        max_length=50,
        db_collation='SQL_Latin1_General_CP1_CI_AS',
        blank=True,
        null=True)
    password = models.CharField(
        max_length=50,
        db_collation='SQL_Latin1_General_CP1_CI_AS',
        blank=True,
        null=True)
    admin = models.BooleanField(blank=True, null=True)
    artist = models.BooleanField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ArtUser'


class Artist(models.Model):
    # Field name made lowercase.
    artistid = models.AutoField(db_column='artistID', primary_key=True)
    first_name = models.CharField(max_length=50,
                                  db_collation='SQL_Latin1_General_CP1_CI_AS')
    last_name = models.CharField(max_length=50,
                                 db_collation='SQL_Latin1_General_CP1_CI_AS')
    email = models.CharField(max_length=50,
                             db_collation='SQL_Latin1_General_CP1_CI_AS')
    phone = models.CharField(max_length=9,
                             db_collation='SQL_Latin1_General_CP1_CI_AS')
    nickname = models.CharField(
        max_length=50,
        db_collation='SQL_Latin1_General_CP1_CI_AS',
        blank=True,
        null=True)
    address = models.CharField(
        max_length=50,
        db_collation='SQL_Latin1_General_CP1_CI_AS',
        blank=True,
        null=True)

    class Meta:
        managed = False
        db_table = 'Artist'


class Customer(models.Model):
    # Field name made lowercase.
    customerid = models.AutoField(db_column='customerID', primary_key=True)
    first_name = models.CharField(max_length=50,
                                  db_collation='SQL_Latin1_General_CP1_CI_AS')
    last_name = models.CharField(max_length=50,
                                 db_collation='SQL_Latin1_General_CP1_CI_AS')
    email = models.CharField(max_length=50,
                             db_collation='SQL_Latin1_General_CP1_CI_AS')
    phone = models.CharField(max_length=9,
                             db_collation='SQL_Latin1_General_CP1_CI_AS')

    class Meta:
        managed = False
        db_table = 'Customer'


class Exhibition(models.Model):
    # Field name made lowercase.
    exhibitionid = models.AutoField(db_column='exhibitionID', primary_key=True)
    starting_date = models.DateField()
    ending_date = models.DateField()
    address = models.CharField(max_length=50,
                               db_collation='SQL_Latin1_General_CP1_CI_AS')
    number_of_visitors = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'Exhibition'


class Orders(models.Model):
    # Field name made lowercase.
    orderid = models.AutoField(db_column='orderID', primary_key=True)
    price = models.DecimalField(max_digits=19, decimal_places=4)
    on_hold = models.BooleanField()
    # Field name made lowercase.
    artid = models.ForeignKey(Artpiece, models.CASCADE, db_column='artID')
    # Field name made lowercase.
    customerid = models.ForeignKey(
        Customer, models.CASCADE, db_column='customerID')

    class Meta:
        managed = False
        db_table = 'Orders'


class Review(models.Model):
    # Field name made lowercase.
    reviewid = models.AutoField(db_column='reviewID', primary_key=True)
    stars = models.IntegerField()
    comment = models.CharField(
        max_length=300,
        db_collation='SQL_Latin1_General_CP1_CI_AS',
        blank=True,
        null=True)
    # Field name made lowercase.
    customerid = models.ForeignKey(
        Customer, models.CASCADE, db_column='customerID')
    # Field name made lowercase.
    artid = models.ForeignKey(Artpiece, models.CASCADE, db_column='artID')

    class Meta:
        managed = False
        db_table = 'Review'


class Reviewhistory(models.Model):
    modified_at = models.DateTimeField(primary_key=True)
    old_stars = models.IntegerField(blank=True, null=True)
    new_stars = models.IntegerField(blank=True, null=True)
    old_comment = models.CharField(
        max_length=300,
        db_collation='SQL_Latin1_General_CP1_CI_AS')
    new_comment = models.CharField(
        max_length=300,
        db_collation='SQL_Latin1_General_CP1_CI_AS')
    # Field name made lowercase.
    customerid = models.ForeignKey(
        Customer, models.CASCADE, db_column='customerID')
    # Field name made lowercase.
    reviewid = models.ForeignKey(Review, models.CASCADE, db_column='reviewID')

    class Meta:
        managed = False
        db_table = 'ReviewHistory'
        unique_together = (('modified_at', 'reviewid'),)


class ExhibitionArtpiece(models.Model):
    # Field name made lowercase.
    artid = models.OneToOneField(
        Artpiece,
        models.CASCADE,
        db_column='artID',
        primary_key=True)
    # Field name made lowercase.
    exhibitionid = models.ForeignKey(
        Exhibition,
        models.CASCADE,
        db_column='exhibitionID')

    class Meta:
        managed = False
        db_table = 'exhibition_artpiece'
        unique_together = (('artid', 'exhibitionid'),)
